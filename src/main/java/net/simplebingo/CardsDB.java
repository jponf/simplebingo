package net.simplebingo;


import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author Josep Pon
 */
public class CardsDB {
    
    /**
     * Creates a new CardsDB with the information of the give file.
     * @param path 
     */
    public CardsDB(String path) throws IOException {
        cardsMap = new HashMap<>();
        loadCards(path);
    }
    
    public Card getCard(Integer id) {
        return cardsMap.get(id);
    }
    
    private void loadCards(String path) throws IOException {
        String fileData = FileUtils.readFileToString(new File(path), 
                                                     (String)null);
        JSONObject obj = new JSONObject(fileData);
        JSONArray cards = obj.getJSONArray(JSON_CARDS);
        
        for (int i = 0; i < cards.length(); ++i) {
            JSONObject card = cards.optJSONObject(i);
            int id = card.optInt(JSON_CARD_ID);
            int[] row1 = jsonArrayToIntArray(card.optJSONArray(JSON_CARD_ROW1));
            int[] row2 = jsonArrayToIntArray(card.optJSONArray(JSON_CARD_ROW2));
            int[] row3 = jsonArrayToIntArray(card.optJSONArray(JSON_CARD_ROW3));
            
            cardsMap.put(id, new Card(row1, row2, row3));
        }        
    }
    
    private int[] jsonArrayToIntArray(JSONArray jsonArray) {
        int[] integers = new int[jsonArray.length()];
        
        for (int i = 0; i < jsonArray.length(); ++i) {
            integers[i] = jsonArray.optInt(i);
        }
        
        return integers;
    }
    
    // Private constants
    private static final String JSON_CARDS = "cards";
    private static final String JSON_CARD_ID = "id";
    private static final String JSON_CARD_ROW1 = "row1";
    private static final String JSON_CARD_ROW2 = "row2";
    private static final String JSON_CARD_ROW3 = "row3";
    
    
    
    // Attributes
    
    private Map<Integer, Card> cardsMap;
}
