package net.simplebingo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

/**
 * Simple generic bingo random number generator.
 * @author Josep Pon
 */
public class SimpleBingoRNG {
    
    public SimpleBingoRNG(int min, int max) {
        if (min > max)
            throw new IllegalArgumentException("Min value > Max Value");
        
        minNumber = min;
        maxNumber = max;
        numbers = new ArrayList<>();
        randEngine = new Random(System.nanoTime());
        
        reset();
    }
    
    /**
     * Refills this random number generator.
     */
    public void reset() {
        numbers.clear();
        
        for (int i = minNumber; i <= maxNumber; ++i)
            numbers.add(i);
        
        Collections.shuffle(numbers, randEngine);
    }
    
    /**
     * Tests whether there are more numbers to extract. 
     * @return True if there are more numbers to extract, false otherwise.
     */
    public boolean hasMoreNumbers() {
        return !numbers.isEmpty();
    }
    
    /**
     * Extracts the next number from this generator.
     * @throws NoSuchElementException If all numbers have already been
     *      extracted.
     * @return The next number.
     */
    public int extractNumber() {
        if (!hasMoreNumbers())
            throw new NoSuchElementException("There are no more numbers to"
                    + " extract");
                    
        int n = numbers.get(numbers.size() - 1);
        numbers.remove(numbers.size() - 1);        
        return n;
    }
    
    private final int minNumber;
    private final int maxNumber;
    private final List<Integer> numbers;
    private final Random randEngine;
}
