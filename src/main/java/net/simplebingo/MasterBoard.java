package net.simplebingo;

import java.util.HashSet;
import java.util.Set;

/**
 * Bingo MasterBoard.
 * @author Josep Pon
 */
public class MasterBoard {

    // ---- Attributes ----
    private final int min;
    private final int max;
    private final Set<Integer> calledNumbers;

    /**
     * Creates a new MasterBoard that accept numbers in the specified range.
     * @param minNumber Minimum accepted number.
     * @param maxNumber Maximum accepted number.
     */
    public MasterBoard(int minNumber, int maxNumber) {
        if (minNumber > maxNumber)
            throw new IllegalArgumentException(
                    "Invalid range of numbers (min > max).");
        
        min = minNumber;
        max = maxNumber;
        calledNumbers = new HashSet<>();
    }

    public void reset() { 
        calledNumbers.clear();
    }
	
    public int getMinNumber() {
        return min;
    }

    public int getMaxNumber() {
        return max;
    }
        
    public boolean hasBeenCalled(int number) {
        return calledNumbers.contains(number);
    }

    public void callNumber(int number) throws AlreadyCalledNumberException {
        if (number < getMinNumber() || number > getMaxNumber()) {
            throw new IllegalArgumentException("The number, " + number
                    + ", is out of the valid range [" + getMinNumber() + "," 
                    + getMaxNumber() + "]");
        } else if (calledNumbers.contains(number)) {
            throw new AlreadyCalledNumberException("The number, " + number 
                    + ", has already been called");
        }
        
        calledNumbers.add(number);    
    }
}
