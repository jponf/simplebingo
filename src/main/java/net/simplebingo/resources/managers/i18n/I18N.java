package net.simplebingo.resources.managers.i18n;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author Josep Pon Farreny
 */
public class I18N {

    // Private constants
    private static final String AUDIO_RESOURCE_PATH = "resources/i18n/audio";
    private static final String STRING_RESOURCE_PATH = "resources/i18n/strings";

    // Attribute
    private Locale locale;
    private ResourceBundle stringsResourceBundle;
    private ResourceBundle audioResourceBundle;

    /**
     * Initializes the internationalization with the default locale.
     */
    public I18N() {
        this(Locale.getDefault());
    }
    
    /**
     * Initializes the internationalization.
     * @param locale Desired locale.
     */
    public I18N(Locale locale) {
        this.locale = locale;
        audioResourceBundle = ResourceBundle.getBundle(
                AUDIO_RESOURCE_PATH, locale);
        stringsResourceBundle = ResourceBundle.getBundle(
                STRING_RESOURCE_PATH, locale);  
    }
    
    public String getString(String key) {
        return stringsResourceBundle.getString(key);
    }
    
    public String getAudioPath(String key) {
        return audioResourceBundle.getString(key);
    }
    
    public Locale getLocale() {
        return locale;
    }
    
    public void setLocale(Locale locale) {
        this.locale = locale;
        audioResourceBundle = ResourceBundle.getBundle(
                AUDIO_RESOURCE_PATH, locale);
        stringsResourceBundle = ResourceBundle.getBundle(
                STRING_RESOURCE_PATH, locale);
        
        System.out.println(this.locale.toString());
        //System.out.println(stringsResourceBundle.getBaseBundleName());
        System.out.println(stringsResourceBundle.getLocale().toString());
    }
}
