package net.simplebingo.resources.managers;

import java.io.IOException;
import java.net.URL;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import net.simplebingo.resources.managers.i18n.I18N;

/**
 * @author Josep Pon Farreny
 */
public class AudioManager {

    private static final String PATH_KEY = "NumbersBasePath";


    /**
     * Creates a new AudioManager instance.
     * @param i18n The internationalisation module that holds the path to the
     * audio files. This reference is not owned by AudioManager.
     */
    public AudioManager(I18N i18n) {
        this.i18n = i18n;
    }
    
    public void sayNumber(int number) {

        try {
            // Open audio input stream.
            String audioPath = String.format("%s/%d.wav", 
                    i18n.getAudioPath(PATH_KEY), number);

            URL url = this.getClass().getClassLoader().getResource(audioPath);
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);

            // Get a sound clip resource.
            Clip clip = AudioSystem.getClip(null);
            
            // Open audio clip and load samples from the audio input stream.
            clip.open(audioIn);
            clip.start();
            
        } catch (UnsupportedAudioFileException | IOException
                | LineUnavailableException e) {
            // These exceptions must be solved in the production phase
            throw new RuntimeException(e);
        }
    }

    // Atributes
    I18N i18n;
}
