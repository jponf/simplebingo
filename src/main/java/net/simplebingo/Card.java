package net.simplebingo;

import java.util.HashSet;
import java.util.Set;

/**
 * European Bingo
 *
 * @author Josep Pon
 */
public class Card {

    public static final int NUM_ROWS = 3;
    public static final int NUM_COLUMNS = 5;

    public Card(int[][] rows) {
        this.rows = rows;
        checkData();
    }

    public Card(int[] row1, int[] row2, int[] row3) {
        this.rows = new int[NUM_ROWS][];
        this.rows[0] = row1;
        this.rows[1] = row2;
        this.rows[2] = row3;
        checkData();
    }

    /**
     * Check if all the numbers in one of the card lines have been called.
     *
     * @param masterBoard
     * @return True if all the numbers of a line have been called, false
     * otherwise.
     */
    public boolean checkLine(MasterBoard masterBoard) {
        return checkLine(rows[0], masterBoard) ||
               checkLine(rows[1], masterBoard) ||
               checkLine(rows[2], masterBoard);
    }

    /**
     * Checks if all the card numbers have been called.
     *
     * @param masterBoard
     * @return True if all them have been called, false otherwise.
     */
    public boolean checkBingo(MasterBoard masterBoard) {
        return checkLine(rows[0], masterBoard) &&
               checkLine(rows[1], masterBoard) &&
               checkLine(rows[2], masterBoard);
    }
    
    // Checks the given line
    private boolean checkLine(int[] row, MasterBoard masterBoard) {
        for (int i = 0; i < NUM_COLUMNS; ++i) {
            if (!masterBoard.hasBeenCalled(row[i]))
                return false;
        }
        return true;
    }

    private void checkData() {
        if (rows == null) {
            throw new CardException("Rows pointer is null");
        }
        if (rows.length != NUM_ROWS) {
            throw new CardException("Wrong number of rows (!= 3)");
        }

        for (int[] r : rows) {
            if (r == null) {
                throw new CardException("One or more rows are null");
            }
            if (r.length != NUM_COLUMNS) {
                throw new CardException("Wrong number of values per row (!= 5)");
            }
        }

        checkValues();
    }

    private void checkValues() {
        Set<Integer> valuesSet = new HashSet<>();
        for (int[] r : rows) {
            for (int v : r) {
                if (!valuesSet.add(v)) {
                    throw new CardException("Repeated values are not allowed");
                }
            }
        }
    }

    private final int[][] rows;
}
