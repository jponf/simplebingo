package net.simplebingo;

/**
 * 
 */
public class AlreadyCalledNumberException extends RuntimeException {

    /**
     * Creates a new AlreadyCalledNumberException without an error message.
     */
    public AlreadyCalledNumberException() {
    }

    /**
     * Creates a new AlreadyCalledNumberException with the given message.
     * @param message
     */
    public AlreadyCalledNumberException(String message) {
            super(message);
    }
}
