package net.simplebingo;

/**
 * @author Josep Pon
 */
public class CardException extends RuntimeException {

    /**
     * Creates a new CardException without an error message.
     */
    public CardException() {
    }

    /**
     * Creates a new CardException with the given message.
     * @param message
     */
    public CardException(String message) {
            super(message);
    }
}
