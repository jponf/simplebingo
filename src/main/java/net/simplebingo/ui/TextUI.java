package net.simplebingo.ui;

import com.googlecode.lanterna.TerminalFacade;
import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.DefaultBackgroundRenderer;
import com.googlecode.lanterna.gui.GUIScreen;
import com.googlecode.lanterna.gui.dialog.ActionListDialog;
import com.googlecode.lanterna.gui.dialog.FileDialog;
import com.googlecode.lanterna.gui.dialog.MessageBox;
import com.googlecode.lanterna.gui.dialog.TextInputDialog;
import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.input.Key.Kind;
import com.googlecode.lanterna.screen.ScreenCharacterStyle;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.TerminalSize;
import java.io.File;
import java.io.IOException;
import net.simplebingo.Card;
import net.simplebingo.CardsDB;
import net.simplebingo.MasterBoard;
import net.simplebingo.SimpleBingoRNG;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;

/**
 *
 * @author Josep Pon
 */
public class TextUI {

    /**
     * Creates a new bingo with a textual interface.
     * @throws java.io.IOException
     */
    public TextUI() throws IOException {
        masterBoard = new MasterBoard(1, 90);
        randomNumberGenerator = new SimpleBingoRNG(masterBoard.getMinNumber(),
                                                   masterBoard.getMaxNumber());
        cardsDB = null;
        lastCalledNumber = 0;
        
        gui = TerminalFacade.createGUIScreen();                
        if (gui == null) {
            throw new RuntimeException("Couldn't allocate a terminal");
        } else {
            gui.getScreen().setCursorPosition(null);
            gui.setBackgroundRenderer(new DefaultBackgroundRenderer("Bingo"));
        }
    }
    
    /**
     * Game main loop.
     */
    public void run() {
        
        gui.getScreen().startScreen();
        try {
            
        
            boolean keepRunning = true;
            while (keepRunning) {            
                Key k = gui.getScreen().readInput();

                if (k != null && k.getKind()== Kind.Escape) {
                    keepRunning = false;
                } else {
                    processInputKey(k);
                    draw();
                }

                try {
                    Thread.sleep(250);
                } catch (InterruptedException ex) {
                }
            }

            
        } catch (Exception e) {
            System.err.println("Exception detected in run: " + e.getMessage());
            System.err.println("Stack trace:");
            e.printStackTrace(System.err);
        }
        
        gui.getScreen().clear();
        gui.getScreen().refresh();
        gui.getScreen().stopScreen();
    }
    
    // Process input
    
    private void processInputKey(Key k) {
        if (k == null)
            return;
        
        System.out.println("Key kind: " + k.getKind().toString());
        
        if (k.getKind()== Kind.NormalKey) {
            switch (Character.toUpperCase(k.getCharacter())) {
                case KEY_CALLNUM:
                    if (randomNumberGenerator.hasMoreNumbers()) {
                        lastCalledNumber = randomNumberGenerator.extractNumber();
                        masterBoard.callNumber(lastCalledNumber);
                    }
                    break;
                case KEY_CHECK:
                    checkCard();
                    break;
                case KEY_RESTART:
                    randomNumberGenerator.reset();
                    masterBoard.reset();
                    break;
                case KEY_LOAD:
                    File f = FileDialog.showOpenFileDialog(gui, 
                            new File(System.getProperty("user.home")),
                            "Open file");
                    loadCardsDB(f.getAbsolutePath());
                    break;
                default:
                    System.out.println("Key character: '" 
                                       + k.getCharacter() + "'");
            }            
        }
    }
    
    // Drawing    
    
    private void draw() {
        gui.getScreen().clear();
                
        TerminalSize size = gui.getScreen().getTerminalSize();
        
        drawMasterBoard(size.getColumns(), size.getRows());
        drawOptions(size.getColumns(), size.getRows());
        gui.getScreen().refresh();
    }
        
    /*private void drawLastCalledNumber() {
        
    }*/
    
    private void drawMasterBoard(int width, int height) {
              
        int BaseX = (width - MASTERBOARD_WIDTH) / 2;
        int BaseY = (int) (height * 0.05);      
        
        for (int i = 0; i < MASTERBOARD_ROWS; ++i) {
            int valueY = BaseY + i * 2 + 1;
            
            drawMasterBoardRowSeparator(BaseX, valueY - 1);
            drawMasterBoardRowSeparator(BaseX, valueY + 1);
            
            for (int j = 0; j < MASTERBOARD_COLUMNS; ++j) {
                int valueX = BaseX + j * 3;
                
                int num = i * MASTERBOARD_COLUMNS + j + 1;  
                
                if (masterBoard.hasBeenCalled(num)) {
                    String strnum = String.format("%1$2d ", num);
                    if (lastCalledNumber == num)
                        drawString(valueX, valueY, strnum, Terminal.Color.RED,
                                Terminal.Color.BLACK, ScreenCharacterStyle.Bold, ScreenCharacterStyle.Blinking);
                    else 
                        drawString(valueX, valueY, strnum, Terminal.Color.WHITE,
                                Terminal.Color.BLACK);
                } else {
                    drawString(valueX, valueY, "   ",
                               Terminal.Color.WHITE, Terminal.Color.BLACK);
                }
            }
        }
        
            
    }
    
    private void drawMasterBoardRowSeparator(int x, int y) {
        drawString(x, y, StringUtils.repeat('-', MASTERBOARD_WIDTH),
                   Terminal.Color.WHITE, Terminal.Color.BLACK);
    }
    
    private void drawOptions(int width, int height) {
        int totalTextSize = CALL_NEXT_NUM_OPTION_KEY.length() + 
                            CALL_NEXT_NUM_OPTION_TXT.length() +
                            CHECK_CARD_KEY.length() + CHECK_CARD_TXT.length() +
                            RESTART_GAME_KEY.length() +
                            RESTART_GAME_TXT.length() +
                            LOAD_CARDS_KEY.length() + LOAD_CARDS_TXT.length() +
                            EXIT_OPTION_KEY.length() + EXIT_OPTION_TXT.length();        
        int totalFreeSpace = width - totalTextSize;
        int freeSpaceAfterOption = totalFreeSpace / NUM_OPTIONS;
        String freeSpaceStr = StringUtils.repeat(" ", freeSpaceAfterOption);
        
        int textX = 0;
        int textY = height - 1;
        
        textX += drawOption(textX, textY, CALL_NEXT_NUM_OPTION_KEY,
                CALL_NEXT_NUM_OPTION_TXT, freeSpaceStr);
        textX += drawOption(textX, textY, CHECK_CARD_KEY, CHECK_CARD_TXT,
                freeSpaceStr);
        textX += drawOption(textX, textY, RESTART_GAME_KEY, RESTART_GAME_TXT,
                freeSpaceStr);
        textX += drawOption(textX, textY, LOAD_CARDS_KEY, LOAD_CARDS_TXT,
                freeSpaceStr);
        textX += drawOption(textX, textY, EXIT_OPTION_KEY, EXIT_OPTION_TXT,
                freeSpaceStr);
    }
    
    private int drawOption(int x, int y, String key, String text, String fill) {
        drawString(x, y, key, Terminal.Color.BLACK, Terminal.Color.WHITE,
                ScreenCharacterStyle.Bold);
        x += key.length();
        drawString(x, y, text + fill, Terminal.Color.BLACK,
                Terminal.Color.CYAN);
        return key.length() + text.length() + fill.length();
    }
    
    // Utility function for lanterna 3.0 alpha, which has been reverted to 2.1.9
    // for stability reasons.
    private void drawString(int x, int y, String str, Terminal.Color fc,
            Terminal.Color bc, ScreenCharacterStyle... styles) {
            gui.getScreen().putString(x, y, str, fc, bc, styles);
    }
    
    // Cards loading and checking
    
    private void loadCardsDB(String path) {
        try {
            cardsDB = new  CardsDB(path);
            MessageBox.showMessageBox(gui, "Success", 
                    "Cards data has been\nsuccessfully loaded");
        } catch (IOException ex) {
            MessageBox.showMessageBox(gui, "I/O Error", ex.getMessage());
        } catch (JSONException ex) {
            MessageBox.showMessageBox(gui, "JSON Error", ex.getMessage());
        }
    }
    
    private void checkCard() {
        if (cardsDB == null) {
            MessageBox.showMessageBox(gui, "Error", "Game cards have not been "
                    + "loaded.\nPlease load it and try again.");
        } else {
            try {
                String strId = TextInputDialog.showTextInputBox(gui, "Card Id", 
                        "Please introduce the card id", "");
                int id = Integer.parseInt(strId);
                
                Card card = cardsDB.getCard(id);
                if (card == null) {
                    MessageBox.showMessageBox(gui, "Card not found", "There is"
                            + " not any card with id '" + strId + "' among the"
                            + " loaded cards");
                } else {
                    checkCard(card);
                }
                
            } catch(NumberFormatException e) {
                MessageBox.showMessageBox(gui, "Error", "The card ID must be an"
                        + " integer number");
            }           
        }
    }
    
    private void checkCard(final Card card) {
        ActionListDialog.showActionListDialog(gui, "Check action", 
                "Select what you want to check",
                new Action() {

                    @Override
                    public void doAction() {
                        String msg;
                        if (card.checkLine(masterBoard))
                            msg = "Line correct :)";
                        else 
                            msg = "Line incorrect :(";
                        MessageBox.showMessageBox(gui, "Line Check", msg);                        
                    }

                    @Override
                    public String toString() {
                        return "Line";
                    }
                },
                new Action() {

                    @Override
                    public void doAction() {
                        String msg;
                        if (card.checkBingo(masterBoard))
                            msg = "Bingo correct!!!";
                        else
                            msg = "Bingo incorrect :(";
                        
                        MessageBox.showMessageBox(gui, "Bingo Check", msg);
                    }
                    
                    @Override
                    public String toString() {
                        return "Bingo!";
                    }
                });
    }
    
    // 
    
    // Private constants
    private static final char KEY_CALLNUM = ' ';
    private static final char KEY_CHECK = 'C';
    private static final char KEY_RESTART = 'R';
    private static final char KEY_LOAD = 'L';
               
    private static final int MASTERBOARD_COLUMNS = 10;
    private static final int MASTERBOARD_ROWS = 9;    
    private static final int MASTERBOARD_WIDTH = 29;
    private static final int MASTERBOARD_HEIGHT = 19;
    
    private static final String CALL_NEXT_NUM_OPTION_KEY = " Space ";
    private static final String CALL_NEXT_NUM_OPTION_TXT = " Call next number";
    private static final String CHECK_CARD_KEY = " C ";
    private static final String CHECK_CARD_TXT = " Check card ";
    private static final String RESTART_GAME_KEY = " R ";
    private static final String RESTART_GAME_TXT = " Restart game"; 
    private static final String LOAD_CARDS_KEY = " L ";
    private static final String LOAD_CARDS_TXT = " Load cards ";
    private static final String EXIT_OPTION_KEY = " Escape ";
    private static final String EXIT_OPTION_TXT = " Quit game";
    
    private static final int NUM_OPTIONS = 5;
    
    // Attributes
    
    private final MasterBoard masterBoard;
    private final SimpleBingoRNG randomNumberGenerator;
    private CardsDB cardsDB;
    private int lastCalledNumber;
      
    private GUIScreen gui;    
}
