package net.simplebingo.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.IOException;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.*;
import javax.swing.border.BevelBorder;
import net.simplebingo.Card;
import net.simplebingo.CardsDB;
import net.simplebingo.MasterBoard;
import net.simplebingo.SimpleBingoRNG;
import net.simplebingo.resources.managers.AudioManager;
import net.simplebingo.resources.managers.i18n.I18N;
import org.json.JSONException;

/**
 *
 * @author Josep Pon
 */
public class GraphicalUI extends JFrame implements ComponentListener,
        ActionListener {


    /**
     * Creates a new GraphicalUI object.
     */
    public GraphicalUI() {
        super(FRAME_TITLE);

        i18n = new I18N(Locale.ENGLISH);
        numbersAudio = new AudioManager(i18n);
        
        masterBoard = new MasterBoard(1, 90);
        randomNumberGenerator = new SimpleBingoRNG(masterBoard.getMinNumber(),
                masterBoard.getMaxNumber());
        cardsDB = null;        
        lastCalledNumber = 0;

        mainPanel = new JPanel();
        controlPanel = new JPanel();
        masterboardPanel = new JPanel(new GridLayout(MASTERBOARD_NUM_ROWS,
                MASTERBOARD_NUM_COLS, 5, 5));

        automaticNumCallTimer = null;
        
        initializeGUI();
    }

    private void initializeGUI() {

        initializeMenuBar();
        initializeMasterboardPanel();
        initializeControlPanel();
        initializeStatusPanel();

        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

        mainPanel.add(controlPanel);
        mainPanel.add(masterboardPanel);
        add(mainPanel);
        
        addComponentListener(this);
        setSize(DEF_SIZE);
    }

    private void initializeMenuBar() {
        menuBar = new JMenuBar();
        
        preferencesMenu = new JMenu(i18n.getString("JMenu.Preferences"));
        preferencesMenu.setMnemonic(preferencesMenu.getText().charAt(0));
        
        // preferences sub-menu sound
        soundMenu = new JMenu(i18n.getString("JMenu.Sound"));
        muteCheckBox = new JCheckBoxMenuItem(
                i18n.getString("JCheckBoxMenuItem.Mute"));
        
        soundMenu.add(muteCheckBox);

        // preferences sub-menu langauge
        languageMenu = new JMenu(i18n.getString("JMenu.Language"));
        ButtonGroup languageButtonsGroup = new ButtonGroup();
        catalanRadioButton = new JRadioButtonMenuItem(
                i18n.getString("JRadioButtonMenuItem.Catalan"));
        catalanRadioButton.addActionListener(this);        
        languageButtonsGroup.add(catalanRadioButton);

        englishRadioButton = new JRadioButtonMenuItem(
                i18n.getString("JRadioButtonMenuItem.English"));
        englishRadioButton.addActionListener(this);
        englishRadioButton.setSelected(true);
        languageButtonsGroup.add(englishRadioButton);
        
        spanishRadioButton = new JRadioButtonMenuItem(
                i18n.getString("JRadioButtonMenuItem.Spanish"));
        spanishRadioButton.addActionListener(this);
        languageButtonsGroup.add(spanishRadioButton);
        
        languageMenu.add(catalanRadioButton);
        languageMenu.add(englishRadioButton);
        languageMenu.add(spanishRadioButton);

        // automatic number calling preferences
        JMenu autoNumCallingMenu = new JMenu("Automatic num. calling period");
        automaticNumberCallPeriodTextField = new JTextField("3.5");
        autoNumCallingMenu.add(automaticNumberCallPeriodTextField);

        preferencesMenu.add(soundMenu);
        preferencesMenu.add(languageMenu);
        preferencesMenu.add(autoNumCallingMenu);
        menuBar.add(preferencesMenu);

        // automatic number calling menu
        automaticNumberCallButton = new JRadioButtonMenuItem(
                i18n.getString("JButton.AutomaticCallNextNumber"));
        automaticNumberCallButton.addActionListener(this);

        menuBar.add(automaticNumberCallButton);

        this.setJMenuBar(menuBar);
    }
    
    private void initializeMasterboardPanel() {
        masterboardLabels = new JLabel[NUM_BALLS];
        for (int i = 0; i < NUM_BALLS; ++i) {
            masterboardLabels[i] = new JLabel(MASTERBOARD_NO_NUM_STRING);
            masterboardLabels[i].setHorizontalAlignment(JLabel.CENTER);

            masterboardPanel.add(masterboardLabels[i]);
        }
    }

    private void initializeControlPanel() {
        controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.X_AXIS));
        
        callNumButton = new JButton(i18n.getString("JButton.CallNextNumber"));
        checkCardButton = new JButton(i18n.getString("JButton.CheckCard"));
        newGameButton = new JButton(i18n.getString("JButton.NewGame"));
        loadCardsButton = new JButton(i18n.getString("JButton.LoadCards"));

        callNumButton.addActionListener(this);
        checkCardButton.addActionListener(this);
        newGameButton.addActionListener(this);
        loadCardsButton.addActionListener(this);

        controlPanel.add(callNumButton);
        controlPanel.add(checkCardButton);
        controlPanel.add(newGameButton);
        controlPanel.add(loadCardsButton);
    }

    private void initializeStatusPanel() {
        JPanel statusPanel = new JPanel();
        statusPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
        statusPanel.setPreferredSize(new Dimension(this.getWidth(), 24));
        statusPanel.setLayout(new BoxLayout(statusPanel, BoxLayout.Y_AXIS));

        statusLabel = new JLabel(FRAME_TITLE);
        statusLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        this.add(statusPanel, BorderLayout.SOUTH);
        statusPanel.add(statusLabel);
    }

    //
    // ActionListener
    //

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource().equals(callNumButton)) {
            callNextNumber();
        } else if (ae.getSource().equals(automaticNumberCallButton)) {
            toggleAutomaticNumberCalling();
        } else if (ae.getSource().equals(checkCardButton)) {
            checkCard();
        } else if (ae.getSource().equals(newGameButton)) {
            newGame();
        } else if (ae.getSource().equals(loadCardsButton)) {
            loadCards();
        // -- Preferences Menu Items --
        } else if (ae.getSource().equals(catalanRadioButton)) {
            changeUILanguage("ca", "ES");
        } else if (ae.getSource().equals(englishRadioButton)) {
            changeUILanguage("en", "GB");
        } else if (ae.getSource().equals(spanishRadioButton)) {
            changeUILanguage("es", "ES");
        }
    }

    private void callNextNumber() {
        if (randomNumberGenerator.hasMoreNumbers()) {
            if (lastCalledNumber > 0) {
                masterboardLabels[lastCalledNumber - 1].setForeground(
                        Color.BLACK);
            }
            
            lastCalledNumber = randomNumberGenerator.extractNumber();
            masterBoard.callNumber(lastCalledNumber);
            
            masterboardLabels[lastCalledNumber - 1].setText(
                    String.valueOf(lastCalledNumber));
            masterboardLabels[lastCalledNumber - 1].setForeground(Color.RED);
            
            setInfoStatusMessage(i18n.getString("JLabel.CalledNumber")
                    + ": " + String.valueOf(lastCalledNumber));
            try {
                if (!muteCheckBox.isSelected())
                    numbersAudio.sayNumber(lastCalledNumber);
            } catch (RuntimeException e) {
                setErrorStatusMessage("Error: " + e.getMessage());
            }
        }
    }

    private void toggleAutomaticNumberCalling() {
        if (automaticNumCallTimer != null) {
            automaticNumCallTimer.cancel();
            automaticNumCallTimer = null;
            automaticNumberCallButton.setSelected(false);
        } else {
            try {
                float p = Float.parseFloat(automaticNumberCallPeriodTextField.getText());
                automaticNumCallTimer = new Timer(true);
                automaticNumCallTimer.scheduleAtFixedRate(new AutoCallNumberTask(), 0,
                        (long)p * 1000);
                automaticNumberCallButton.setSelected(true);
            } catch (NumberFormatException e) {
                setErrorStatusMessage("Error: " + e.getMessage());
            }
        }
    }

    private void checkCard() {
        if (cardsDB == null) {
            setAlertStatusMessage(
                    i18n.getString("JLabel.GameCardsNotLoadedWarning"));
        } else {
            try {
                String strId = JOptionPane.showInputDialog(
                        i18n.getString("JOptionPane.IntroduceCardID")).trim();
                int id = Integer.parseInt(strId);
                Card card = cardsDB.getCard(id);
                if (card == null) {
                    setAlertStatusMessage("There is not any card with id '" 
                            + strId + "' among the loaded cards");
                } else {
                    checkCard(card);
                }
            } catch (NumberFormatException ex) {
                setErrorStatusMessage("The card ID must be an integer number");
            }
        }       
    }    
    
    private void checkCard(final Card card) {
        Object[] options = { i18n.getString("JOptionPane.OptionLine"),
                i18n.getString("JOptionPane.OptionBingo") };
        int selected = JOptionPane.showOptionDialog(this,
                i18n.getString("JOptionPane.SelectCheckOption"), "Check Dialog",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                options, options[1]);
   
        String checkMessage = "";
        switch (selected) {
            case 0:
                checkMessage = card.checkLine(masterBoard) ?
                        i18n.getString("JOptionPane.AlertLineCorrect") :
                        i18n.getString("JOptionPane.AlertLineIncorrect");
                break;
            case 1:
                checkMessage = card.checkBingo(masterBoard) ?
                        i18n.getString("JOptionPane.AlertBingoCorrect") :
                        i18n.getString("JOptionPane.AlertBingoIncorrect");          
                break;
            case JOptionPane.CLOSED_OPTION:                
                setInfoStatusMessage(
                        i18n.getString("JLabel.CardCheckCanceled"));
                return;
            default:
                throw new RuntimeException("Unexpected option");
        }
        
        setAlertStatusMessage(checkMessage);
        JOptionPane.showMessageDialog(this, checkMessage);
    }
    
    private void newGame() {
        for (int i = 0; i < NUM_BALLS; ++i) {
            masterboardLabels[i].setText(MASTERBOARD_NO_NUM_STRING);
            masterboardLabels[i].setForeground(Color.BLACK);
            masterboardLabels[i].repaint();
        }
        
        lastCalledNumber = 0;
        randomNumberGenerator.reset();
        masterBoard.reset();
        
        setInfoStatusMessage(i18n.getString("JLabel.NewGame"));
        
        this.repaint();
    }
    
    private void loadCards() {
        JFileChooser fc = new JFileChooser();
        int returnVal = fc.showOpenDialog(this);
        
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                String path = fc.getSelectedFile().getPath();
                cardsDB = new CardsDB(path);
                setInfoStatusMessage("Loaded cards - " + path);
            } catch (IOException ex) {
                setErrorStatusMessage("I/O Error: " + ex.getMessage());
            } catch (JSONException ex) {
                setErrorStatusMessage("JSON Error: " + ex.getMessage());
            }
        } else {
            setInfoStatusMessage("Load cards cancelled");
        }
    }
    
    private void changeUILanguage(String language, String country) {
        Locale newLocale = new Locale(language, country);
        i18n.setLocale(newLocale);
        
        preferencesMenu.setText(i18n.getString("JMenu.Preferences"));
        soundMenu.setText(i18n.getString("JMenu.Sound"));
        muteCheckBox.setText(i18n.getString("JCheckBoxMenuItem.Mute"));
        
        languageMenu.setText(i18n.getString("JMenu.Language"));
        catalanRadioButton.setText(
                i18n.getString("JRadioButtonMenuItem.Catalan"));
        englishRadioButton.setText(
                i18n.getString("JRadioButtonMenuItem.English"));
        spanishRadioButton.setText(
                i18n.getString("JRadioButtonMenuItem.Spanish"));
        
        callNumButton.setText(i18n.getString("JButton.CallNextNumber"));
        checkCardButton.setText(i18n.getString("JButton.CheckCard"));
        newGameButton.setText(i18n.getString("JButton.NewGame"));
        loadCardsButton.setText(i18n.getString("JButton.LoadCards"));
    }

    //
    // JFrame ComponentListener
    //

    @Override
    public void componentResized(ComponentEvent ce) {
        //System.err.println("-- Component resized");
        Component c = ce.getComponent();
        resizeGUIComponents(c.getWidth(), c.getHeight());
    }

    @Override
    public void componentMoved(ComponentEvent ce) {
        //System.err.println("-- Component moved"); // Does nothing
    }

    @Override
    public void componentShown(ComponentEvent ce) {
        //System.err.println("-- Component shown");
        Component c = ce.getComponent();
        resizeGUIComponents(c.getWidth(), c.getHeight());
    }

    @Override
    public void componentHidden(ComponentEvent ce) {
        //System.err.println("-- Component hidden"); // Does nothing
    }

    //
    // Resize components methods
    //
    
    private void resizeGUIComponents(int width, int height) {
        resizeMasterboardGUIElements(width, height);
        resizeControlPanelGUIElements(width, height);
    }

    private void resizeMasterboardGUIElements(int width, int height) {
        masterboardPanel.setPreferredSize(
                new Dimension(width, (int) Math.round(height * 0.8)));

        int mboardWidth = masterboardPanel.getWidth()
                - MASTERBOARD_XGAP * (MASTERBOARD_NUM_COLS - 1);
        int mboardHeight = masterboardPanel.getHeight()
                - MASTERBOARD_YGAP * (MASTERBOARD_NUM_ROWS - 1);
        Dimension cellSize = new Dimension(
                Math.round(mboardWidth / MASTERBOARD_NUM_COLS),
                Math.round(mboardHeight / MASTERBOARD_NUM_ROWS));
        resizeMasterBoardGUICell(cellSize);
    }

    private void resizeMasterBoardGUICell(Dimension cellSize) {
        for (int i = 0; i < NUM_BALLS; ++i) {
            JLabel label = masterboardLabels[i];
            Font font = label.getFont();

            // Each cell will contain a string with at most two characters
            // the width computed using 3 characters guarantees that all text
            // will be visible
            double strWidth = label.getFontMetrics(font).stringWidth("---");
            double widthRatio = cellSize.getWidth() / (strWidth + 5);

            double preSize = font.getSize() * widthRatio;
            double newSize = Math.min(preSize, cellSize.getHeight());

            label.setFont(font.deriveFont((float) newSize));
        }
    }

    private void resizeControlPanelGUIElements(int width, int height) {
    }
    
    //-----
    
    private void setErrorStatusMessage(String message) {
        statusLabel.setForeground(Color.red);
        statusLabel.setText(message);
    }
    
    private void setInfoStatusMessage(String message) {
        statusLabel.setForeground(Color.black);
        statusLabel.setText(message);
    }
    
    private void setAlertStatusMessage(String message) {
        statusLabel.setForeground(Color.blue);
        statusLabel.setText(message);
    }


    //-----

    private class AutoCallNumberTask extends TimerTask {

        @Override
        public void run() {
            callNextNumber();
        }
    }
    
    // private static final attributes
    private static final String FRAME_TITLE = "SimpleBingo";
    private static final Dimension DEF_SIZE = new Dimension(640, 640);

    private static final int MASTERBOARD_NUM_ROWS = 9;
    private static final int MASTERBOARD_NUM_COLS = 10;
    private static final int NUM_BALLS = MASTERBOARD_NUM_ROWS
            * MASTERBOARD_NUM_COLS;
    private static final int MASTERBOARD_XGAP = 5;
    private static final int MASTERBOARD_YGAP = 5;
    private static final String MASTERBOARD_NO_NUM_STRING = "--";

    // Attributes
    private final I18N i18n;
    private final AudioManager numbersAudio;
    
    private final MasterBoard masterBoard;
    private final SimpleBingoRNG randomNumberGenerator;
    private CardsDB cardsDB;
    private int lastCalledNumber;
    
    // Graphical elements
    private final JPanel mainPanel;
    private final JPanel controlPanel;
    private final JPanel masterboardPanel;
    
    private JMenuBar menuBar;
    private JMenu preferencesMenu;
    private JMenu soundMenu;
    private JCheckBoxMenuItem muteCheckBox;
    private JMenu languageMenu;
    private JRadioButtonMenuItem catalanRadioButton;
    private JRadioButtonMenuItem englishRadioButton;
    private JRadioButtonMenuItem spanishRadioButton;
    private JRadioButtonMenuItem automaticNumberCallButton;
    private JTextField automaticNumberCallPeriodTextField;

    private JButton callNumButton;
    private JButton checkCardButton;
    private JButton newGameButton;
    private JButton loadCardsButton;

    private JLabel[] masterboardLabels;

    private JLabel statusLabel;

    // Automatic call timer
    Timer automaticNumCallTimer;
}
