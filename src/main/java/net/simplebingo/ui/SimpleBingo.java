package net.simplebingo.ui;

import java.io.IOException;
import javax.swing.JFrame;

public class SimpleBingo {

    public static void main(String[] args) {

        CommandLineOptions options = new CommandLineOptions();        
        options.parseCommandLine(args);
       
        if (options.hasOption(CommandLineOptions.NOGUI)) {
            try {
                TextUI tui = new TextUI();
            
                tui.run();
            } catch (IOException ex) {
                System.err.println("I/O Error: " + ex.getMessage());
            }
        } else {
            createAndShowGUI();            
        }
    }
    
    private static void createAndShowGUI()
    {
        GraphicalUI gui = new GraphicalUI();
        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //gui.initialize();
        gui.setVisible(true);            
    }
}
