package net.simplebingo.ui;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 *
 * @author Josep Pon
 */
public class CommandLineOptions {

    // List of avaliable options
    public static final String NOGUI = "nogui";
    
    /**
     * Creates a new CommandLineOptions object.
     */
    public CommandLineOptions() {
        // Create options
        options = new Options();
        
        createOptions();
    }
    
    /**
     * Parses the given arguments.
     * @param args 
     */
    public void parseCommandLine(String[] args) {
        try {
            CommandLineParser cmdlp = new BasicParser();
            cmdl = cmdlp.parse(options, args);
        } catch (ParseException ex) {
            throw new RuntimeException("Parsing failed. Reason: " 
                                       + ex.getMessage());
        }
    }
    
    /**
     * Query to see if an option has been set.
     * @param opt 
     * @return True if the option was set, false otherwise.
     */
    public boolean hasOption(String opt) {
        return cmdl.hasOption(opt);
    }
        
    /**
     * 
     */
    private void createOptions() {
        Option gui = new Option(NOGUI, 
                                "Runs the game without graphical interface");
        options.addOption(gui);
    }
    
    
    private final Options options;    
    private CommandLine cmdl;
}
