#!/usr/bin/env python

import re, sys

if __name__ == '__main__':

    f = file(sys.argv[1], "r")
    data = f.read()
    f.close()

    # process data
    out = file("cardtojson.out", "w")
    
#Card 1:
#1 23 44 61 80
#16 36 58 68 84
#28 49 59 71 89

    regex = re.compile("Card\s*(?P<id>\d+):\n(?P<v11>\d+)\s*(?P<v12>\d+)\s*(?P<v13>\d+)\s*(?P<v14>\d+)\s*(?P<v15>\d+)\s*\n(?P<v21>\d+)\s*(?P<v22>\d+)\s*(?P<v23>\d+)\s*(?P<v24>\d+)\s*(?P<v25>\d+)\s*(?P<v31>\d+)\s*(?P<v32>\d+)\s*(?P<v33>\d+)\s*(?P<v34>\d+)\s*(?P<v35>\d+)")

    matches = regex.findall(data)
    print "Number of matches:", len(matches)

    print >> out, '{'
    print >> out, '    "cards": ['

    for m in matches:
        #print >> out, m
        print >> out, '        {'
        print >> out, '            "id": "%s",' % (m[0])
        print >> out, '            "row1": [%s, %s, %s, %s, %s],' % (m[1], m[2], m[3], m[4], m[5])
        print >> out, '            "row2": [%s, %s, %s, %s, %s],' % (m[6], m[7], m[8], m[9], m[10])
        print >> out, '            "row3": [%s, %s, %s, %s, %s]'  % (m[11], m[12], m[13], m[14], m[15])  
        print >> out, '        },'

    print >> out, '    ]'
    print >> out, '}'

    out.close()
    
